#### 自动部署平台说明文档 

# 说明
```$xslt

1、本项目在walle部署系统1.0版本的基础上，使用 hyperf+vue 做了重构，感谢前人的努力！！
2、技术栈 Hyperf(Swoole版的Laravel，前端iview,PHP、多进程、websocket)
3、内部测试中，暂未发现问题，若发现bug请联系我，同时可以提一些意见
4、一开始没想到数据库表迁移，就没有做migration了，sql需要手动导入,sql文件在项目目录/install 下，后面考虑加上 哈哈！！

```
# 体验地址
```
http://autods.easyapi.site/

demo/123456
```
# 页面展示

![image](https://images.gitee.com/uploads/images/2020/0618/182504_2def221e_505775.png)

![image](http://gitee.easyapi.site/images/project_list.png)

![image](http://gitee.easyapi.site/images/project_edit.png)

![image](http://gitee.easyapi.site/images/task.png)

![image](https://images.gitee.com/uploads/images/2020/0618/182523_f4184f48_505775.png)

![image](http://gitee.easyapi.site/images/deploy.png)

![image](http://gitee.easyapi.site/images/permission.png)



# 开发初衷

```$xslt
walle1.0很稳定但是很多功能不支持，同时又很喜欢walle2.0的界面，但是walle2.0目前不稳定，索性就自己动手写一个，1.0的基础上做了页面的重构，同时新增了以下功能

1、媲美walle2.0的UI界面
2、比walle2.0强大的权限管理
3、保留walle1.0的操作习惯与稳定性
4、支持灰度发布
5、支持阿里云SLB，动态获取负载均衡IP

```
# 环境搭建-本地快速部署(docker版本)

```
1，git clone xxxx autods-server 代码到桌面    (方便后面直接改代码)

2，docker run -v /桌面/autods-server:/autods-server -p 9501:9501 -it --entrypoint /bin/sh hyperf/hyperf:7.2-alpine-cli

3，进入 上一步新建的容器 docker exec -it xxx  /bin/bash 
     cd /autods-server
     composer install   
     配置env 数据库、redis常规配置 
     php watch     

3，git clone xxx autods-vue 到桌面

4，cd autods-vue   修改 .env.development配置  
        VUE_TARGET_URL ='http://127.0.0.1:9501/api/'
        cnpm  run dev

5，浏览器打开  登录  账号：admin 密码：123456


```

# 环境搭建-远端部署

```$xslt
1.PHP基础环境
2.swoole扩展（PHP短标签需要关闭）
3.composer install
4.cp .env.exapmle .env
5.填写数据库信息、redis信息，如果需要使用阿里云负载均衡 配置响应信息

6.启动
php  /path/to/autods/bin/hyperf.php start   
建议在开发时，使用热重启 php watch

7.每次部署代码时，需要重新启动  

```

# nginx 代理

```$xslt
## 后端nginx配置
server
{
    listen 80;
    server_name server后端域名;

    location /api/ {
        proxy_pass http://127.0.0.1:9501;#此端口修改的话，项目中的http端口也需要修改
        proxy_set_header Host $proxy_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Via "nginx";

        # 允许前端跨域 
        add_header Access-Control-Allow-Origin *;
        add_header Access-Control-Allow-Methods 'GET, POST, OPTIONS';
        add_header Access-Control-Allow-Headers 'DNT,Keep-Alive,User-Agent,Cache-Control,Content-Type,Authorization,X-Token';
 
        # vue项目 每次请求之前会发一个options
        if ($request_method = 'OPTIONS') {
         return 204;
        }

    }   

    access_log  /www/wwwlogs/autods.site.log;
    error_log  /www/wwwlogs/autods.error.log;
}

```

```$xslt
## 前端nginx
server
{
    listen 80;
    server_name server前端域名;

    location / {
        proxy_pass http://server后端域名;#此端口修改的话，项目中的http端口也需要修改
        proxy_set_header Host $proxy_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Via "nginx";
    }   

    access_log  /www/wwwlogs/autods.site.log;
    error_log  /www/wwwlogs/autods.error.log;
}

```

# 交流
> QQ群

![image](http://gitee.easyapi.site/qq.jpeg)

> Wechat群

![image](http://gitee.easyapi.site/wechat1.jpeg)

> 个人微信

![image](http://gitee.easyapi.site/me.jpeg)
