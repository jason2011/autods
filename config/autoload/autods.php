<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.04.10
 */

return [
    'ansible_hosts_dir' => BASE_PATH . '/runtime/ansible_hosts/',
    'mail-suffix'       => [
        'qmai.cn',
    ],

    'user_driver' => 'ldap',

    'ldap' => [
        'host'           => '',
        'port'           => '',
        'accountBase'    => '',
        'accountPattern' => '',
        'identity'       => '',
        'attributesMap'  => [
            'cn'          => 'username',
            'mail'        => 'email',
            'displayName' => 'realname',
        ],
        'ssl'            => false,
    ],
];