<?php

declare(strict_types=1);

use Hyperf\HttpServer\Router\Router;

//权限检测
$middleware = [
    App\Middleware\AuthMiddleware::class,
    App\Middleware\PermissionMiddleware::class,
];

//默认首页
Router::addRoute(['get', 'post'], '/', [App\Controller\Http\IndexController::class, 'index']);


Router::post('/api/login', [App\Controller\Http\LoginController::class, 'login']);
Router::post('/api/logout', [App\Controller\Http\LoginController::class, 'logout']);
Router::get('/api/dashboard', [App\Controller\Http\DashboardController::class, 'dashboard']);

Router::addGroup('/api/', function () {

    /*
     |--------------------------------------------------------------------------
     | workspace管理 第三个参数是用于权限校验的
     |--------------------------------------------------------------------------
     */
    Router::get('workspace', [App\Controller\Http\WorkSpaceController::class, 'index', 'workspace.list']);
    Router::post('workspace', [App\Controller\Http\WorkSpaceController::class, 'store', 'workspace.add']);
    Router::put('workspace/{id:\d+}', [App\Controller\Http\WorkSpaceController::class, 'update', 'workspace.update']);
    Router::get('workspace/{id:\d+}', [App\Controller\Http\WorkSpaceController::class, 'show', 'workspace.show']);
    Router::delete('workspace/{id:\d+}', [App\Controller\Http\WorkSpaceController::class, 'delete', 'workspace.delete']);
    Router::get('workspace/user', [App\Controller\Http\WorkSpaceController::class, 'workspaceUser', 'workspace.user.list']);
    Router::post('workspace/user', [App\Controller\Http\WorkSpaceController::class, 'userStore', 'workspace.user.add']);
    Router::delete('workspace/user/{id:\d+}', [App\Controller\Http\WorkSpaceController::class, 'userDelete', 'workspace.user.delete']);

    /*
    |--------------------------------------------------------------------------
    | 项目管理
    |--------------------------------------------------------------------------
    */
    Router::get('projects', [App\Controller\Http\ProjectController::class, 'index', 'project.list']);
    Router::post('projects', [App\Controller\Http\ProjectController::class, 'store', 'project.add']);
    Router::post('projects/copy/{id:\d+}', [App\Controller\Http\ProjectController::class, 'copy', 'project.copy']);
    Router::put('projects/{id:\d+}', [App\Controller\Http\ProjectController::class, 'update', 'project.update']);
    Router::get('projects/{id:\d+}', [App\Controller\Http\ProjectController::class, 'show', 'project.show']);
    Router::delete('projects/{id:\d+}', [App\Controller\Http\ProjectController::class, 'delete', 'project.delete']);
    Router::post('projects/detection/{id:\d+}', [App\Controller\Http\ProjectController::class, 'detection', 'project.detection']);
    Router::post('projects/md5', [App\Controller\Http\ProjectController::class, 'mdfCheck', 'project.md5']);
    Router::get('projects/all', [App\Controller\Http\ProjectController::class, 'all', 'project.all']);
    Router::post('projects/slbPort', [App\Controller\Http\ProjectController::class, 'slbPort', 'project.slb']);


    /*
   |--------------------------------------------------------------------------
   | Task管理
   |--------------------------------------------------------------------------
   */
    Router::get('projects/classification', [App\Controller\Http\ProjectController::class, 'projectClassification', 'task.project.classification']);
    Router::get('task', [App\Controller\Http\TaskController::class, 'index', 'task.list']);
    Router::post('task', [App\Controller\Http\TaskController::class, 'store', 'task.add']);
    Router::put('task/{id:\d+}', [App\Controller\Http\TaskController::class, 'update', 'task.update']);
    Router::get('task/{id:\d+}', [App\Controller\Http\TaskController::class, 'show', 'task.show']);
    Router::delete('task/{id:\d+}', [App\Controller\Http\TaskController::class, 'delete', 'task.delete']);

    Router::get('task/tag', [App\Controller\Http\TaskController::class, 'tag', 'task.branch']);
    Router::get('task/branch', [App\Controller\Http\TaskController::class, 'branch', 'task.branch']);
    Router::get('task/version', [App\Controller\Http\TaskController::class, 'version', 'task.version']);
    Router::post('task/audit', [App\Controller\Http\TaskController::class, 'audit', 'task.audit']);
    Router::post('task/rollback/{id:\d+}', [App\Controller\Http\TaskController::class, 'rollback', 'task.rollback']);
    Router::post('task/deploy/{id:\d+}', [App\Controller\Http\TaskController::class, 'deploy', 'task.deploy']);

    /*
   |--------------------------------------------------------------------------
   | 环境管理
   |--------------------------------------------------------------------------
   */
    Router::get('environment', [App\Controller\Http\EnvironmentController::class, 'index', 'environment.list']);
    Router::post('environment', [App\Controller\Http\EnvironmentController::class, 'store', 'environment.add']);
    Router::put('environment/{id:\d+}', [App\Controller\Http\EnvironmentController::class, 'update', 'environment.update']);
    Router::delete('environment/{id:\d+}', [App\Controller\Http\EnvironmentController::class, 'delete', 'environment.delete']);

    /*
    |--------------------------------------------------------------------------
    | User管理
    |--------------------------------------------------------------------------
    */
    Router::get('users', [App\Controller\Http\UserController::class, 'users', 'user.list']);
    Router::post('user-info', [App\Controller\Http\UserController::class, 'info', 'system.user.info']);
    Router::post('users-list', [App\Controller\Http\UserController::class, 'list', 'user.list']);
    Router::post('users', [App\Controller\Http\UserController::class, 'store', 'user.add']);
    Router::put('users/{id:\d+}', [App\Controller\Http\UserController::class, 'update', 'user.update']);
    Router::delete('users/{id:\d+}', [App\Controller\Http\UserController::class, 'delete', 'user.delete']);
    Router::post('change', [App\Controller\Http\UserController::class, 'change', 'user.change']);

    /*
    |--------------------------------------------------------------------------
    | Role管理
    |--------------------------------------------------------------------------
    */
    Router::get('roles', [App\Controller\Http\RoleController::class, 'index', 'roles.list']);
    Router::put('roles/{id:\d+}', [App\Controller\Http\RoleController::class, 'update', 'roles.update']);
    Router::post('roles/permissions/{id:\d+}', [App\Controller\Http\RoleController::class, 'permission', 'roles.permission']);
    Router::get('roles/permissions/{id:\d+}', [App\Controller\Http\RoleController::class, 'getPermissions', 'roles.permission']);

    /*
    |--------------------------------------------------------------------------
    | Permission管理
    |--------------------------------------------------------------------------
    */
    Router::get('permissions', [App\Controller\Http\PermissionController::class, 'index', 'permission.list']);
    Router::get('top/class', [App\Controller\Http\PermissionController::class, 'top', 'permission.top']);
    Router::post('permissions', [App\Controller\Http\PermissionController::class, 'store', 'permission.add']);
    Router::put('permissions/{id:\d+}', [App\Controller\Http\PermissionController::class, 'update', 'permission.update']);
    Router::delete('permissions/{id:\d+}', [App\Controller\Http\PermissionController::class, 'delete', 'permission.delete']);


}, ['middleware' => $middleware]);


/*
 |--------------------------------------------------------------------------
 | Websocket路由
 |--------------------------------------------------------------------------
 */

Router::addServer('ws', function () {
    Router::get('/', 'App\Controller\WebSocket\IndexController');
});