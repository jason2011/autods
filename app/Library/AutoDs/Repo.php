<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.04.10
 */

namespace App\Library\AutoDs;

use App\Model\Project;
use ErrorException;

class Repo extends Command
{
    /**
     * 获取版本管理句柄
     *
     * @param $conf
     * @throws ErrorException
     * @return Git|Svn
     */
    public static function getInstance($conf)
    {
        switch ($conf->repo_type) {
            case Project::REPO_GIT:
                return new Git($conf);
            case Project::REPO_SVN:
                return new Svn($conf);
            default:
                throw new ErrorException(t('message.12064'));
                break;
        }
    }
}
