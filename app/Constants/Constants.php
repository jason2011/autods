<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.04.20
 */

namespace App\Constants;

class Constants
{
    const RETURN_SUCCESS = 200;

    const SERVER_ERROR = 500;

    const X_TOKEN = 'X-Token';

    const TOKEN = 'token';

    const PERMISSION_DENIED = 'Permission denied';

    // 系统超级管理员id
    const SYS_ADMIN_ID = 1;
}
