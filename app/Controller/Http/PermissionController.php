<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.03.30
 */

namespace App\Controller\Http;

use App\Constants\Constants;
use App\Library\Traits\Helper;
use App\Service\PermissionService;
use Hyperf\Di\Annotation\Inject;

class PermissionController extends BaseController
{
    /**
     * @Inject()
     * @var PermissionService
     */
    public $service;

    /**
     * 权限列表
     */
    public function index()
    {
        try {
            $result = $this->service->tree();
            return $this->response->showResults(Constants::RETURN_SUCCESS, 'SUCCESS', $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 一级分类
     */
    public function top()
    {
        try {
            $result = $this->service->top();
            return $this->response->showResults(Constants::RETURN_SUCCESS, 'SUCCESS', $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 新增权限
     */
    public function store()
    {
        try {
            $rules = [
                'parent_id'    => 'required',
                'name'         => 'required',
                'display_name' => 'required',
                'type'         => 'required',
                'sort'         => 'required',
            ];
            $this->validator($rules);
            $data = $this->request->all();
            $this->service->add($data);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 更新权限
     */
    public function update()
    {
        try {
            $rules = [
                'parent_id'    => 'required',
                'name'         => 'required',
                'display_name' => 'required',
                'type'         => 'required',
                'sort'         => 'required',
            ];
            $this->validator($rules);
            $data = $this->request->all();
            $this->service->edit($data);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 删除权限
     */
    public function delete()
    {
        try {
            $rules = ['id' => 'required'];
            $this->validator($rules);
            $id = $this->request->input('id');
            $this->service->delete($id);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }
}
