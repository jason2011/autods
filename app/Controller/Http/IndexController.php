<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.05.11
 */

namespace App\Controller\Http;

use App\Controller\AbstractController;

class IndexController extends AbstractController
{

    public function index()
    {
        try {
            $data = [
                'Hello,AutoDs!!'
            ];
            return $this->response->showResults(200, 'SUCCESS', $data);
        } catch (\Exception $exception) {
            return $this->response->showResults(500, $exception->getMessage());
        }
    }
}
