<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.03.30
 */

namespace App\Controller\Http;

use App\Controller\AbstractController;
use App\Library\Traits\Helper;
use Hyperf\Di\Annotation\Inject;
use Redis;

class BaseController extends AbstractController
{
    use Helper;

    /**
     * @Inject()
     * @var Redis
     */
    public $redis;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->redis = redis();
    }
}
