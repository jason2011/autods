<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.04.01
 */

namespace App\Controller\Http;

use App\Constants\Constants;
use App\Library\Traits\Helper;
use App\Service\WorkSpaceService;
use Hyperf\Di\Annotation\Inject;

class WorkSpaceController extends BaseController
{
    /**
     * @Inject
     * @var WorkSpaceService
     */
    protected $service;

    /**
     * 部门列表
     */
    public function index()
    {
        try {
            $result = $this->service->query();
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 新建部门
     */
    public function store()
    {
        try {
            $rules = [
                'name'   => 'required',
                'owner'  => 'required',
                'master' => 'required',
            ];
            $this->validator($rules);
            $data = $this->combine(array_keys($rules));
            $this->service->add($data);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 更新部门
     */
    public function update()
    {
        try {
            $rules = [
                'id'     => 'required',
                'name'   => 'required',
                'owner'  => 'required',
                'master' => 'required',
            ];
            $this->validator($rules);
            $data = $this->combine(array_keys($rules));
            $this->service->edit($data);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 删除部门
     */
    public function delete()
    {
        try {
            $rules = ['id' => 'required'];
            $this->validator($rules);
            $id = $this->request->input('id');
            $this->service->delete($id);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 新增部门用户
     */
    public function userStore()
    {
        try {
            $rules = [
                'user_id'      => 'required',
                'workspace_id' => 'required',
                'role_id'      => 'required',
            ];
            $this->validator($rules);
            $data = $this->combine(array_keys($rules));
            $this->service->addUser($data);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 获取部门用户
     */
    public function workspaceUser()
    {
        try {
            $page     = $this->request->input('page', 1);
            $pageSize = $this->request->input('pageSize', 10);

            $workspaceId = $this->request->input('workspace_id', 0);
            $searchKey   = $this->request->input('search_key', '');
            $searchValue = $this->request->input('search_value', '');
            $condition   = [];
            if ($workspaceId) {
                $condition['workspace_id'] = $workspaceId;
            }

            if ($searchKey && $searchValue) {
                $key         = $searchKey == 'name' ? 'c.name' : 'b.nick_name';
                $condition[] = [$key, 'like', '%' . trim($searchValue) . '%'];
            }

            $result = $this->service->queryUser($page, $pageSize, $condition);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 删除用户
     */
    public function userDelete()
    {
        try {
            $rules = ['id' => 'required'];
            $this->validator($rules);
            $id = $this->request->input('id');
            $this->service->deleteUser($id);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }
}
