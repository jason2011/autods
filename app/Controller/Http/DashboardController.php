<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.05.08
 */

namespace App\Controller\Http;

use App\Constants\Constants;
use Psr\Http\Message\ResponseInterface;

class DashboardController extends BaseController
{
    /**
     * dashboard首页
     *
     * @return ResponseInterface
     */
    public function dashboard()
    {
        try {
            $result = [
                'info_data' => [
                    [
                        'title' => '用户数',
                        'icon'  => 'md-person-add',
                        'count' => 132,
                        'color' => '#2d8cf0',
                    ],
                    [
                        'title' => '项目数',
                        'icon'  => 'md-share',
                        'count' => 78,
                        'color' => '#ed3f14',
                    ],
                    [
                        'title' => '发布数',
                        'icon'  => 'md-send',
                        'count' => 190,
                        'color' => '#19be6b',
                    ],
                    [
                        'title' => '部门数',
                        'icon'  => 'md-home',
                        'count' => 10,
                        'color' => '#ff9900',
                    ]
                ],

                'pie_data' => [
                    [
                        'value' => 10,
                        'name'  => 'iot事业部',
                    ],
                    [
                        'value' => 12,
                        'name'  => '大数据中心',
                    ],
                    [
                        'value' => 16,
                        'name'  => '企小店',
                    ],
                    [
                        'value' => 22,
                        'name'  => '独立事业部',
                    ],
                    [
                        'value' => 87,
                        'name'  => '订单中心',
                    ]
                ],

                'bar_data' => [
                    'Mon' => 0,
                    'Tue' => 31,
                    'Wed' => 3,
                    'Thu' => 30,
                    'Fri' => 5,
                    'Sat' => 2,
                    'Sun' => 1
                ],
            ];
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }
}
