<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.04.03
 */

namespace App\Controller\Http;

use App\Constants\Constants;
use App\Library\Traits\Helper;
use App\Service\ProjectService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Utils\Context;

class ProjectController extends BaseController
{
    /**
     * @Inject()
     * @var  ProjectService
     */
    protected $service;

    protected $user;

    /**
     * ProjectController constructor.
     */
    public function __construct()
    {
        $this->user = Context::get('user');

        parent::__construct();
    }

    /**
     * 列表
     */
    public function index()
    {
        try {
            $page        = $this->request->input('page', 1);
            $pageSize    = $this->request->input('pageSize', 10);
            $searchKey   = $this->request->input('search_key', '');
            $searchValue = $this->request->input('search_value', '');
            $condition   = [];
            if ($searchKey && $searchValue) {
                $key         = 'project.name';
                $condition[] = [$key, 'like', '%' . trim($searchValue) . '%'];
            }
            $result = $this->service->query($this->user->user_id, $page, $pageSize, $condition);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 当前用户下的所有负责项目（不分页）
     */
    public function all()
    {
        try {
            $result = $this->service->all();
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 明细
     */
    public function show()
    {
        try {
            $rules = ['id' => 'required'];
            $this->validator($rules);
            $id     = $this->request->input('id');
            $result = $this->service->show($id);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 新增
     */
    public function store()
    {
        try {
            $rules = [
                'name'             => 'required',
                'workspace_id'     => 'required',
                'level'            => 'required',
                'repo_url'         => 'required',
                'repo_mode'        => 'required',
                'deploy_from'      => 'required',
                'release_user'     => 'required',
                'release_to'       => 'required',
                'release_library'  => 'required',
                'keep_version_num' => 'required',
            ];
            $this->validator($rules);
            $data = $this->request->all();
            $this->service->add($data);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 拷贝
     */
    public function copy()
    {
        try {
            $rules = ['id' => 'required',];
            $this->validator($rules);
            $id = $this->request->input('id');
            $this->service->copy($id);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 更新
     */
    public function update()
    {
        try {
            $rules = [
                'id'               => 'required',
                'user_id'          => 'required',
                'name'             => 'required',
                'workspace_id'     => 'required',
                'level'            => 'required',
                'repo_url'         => 'required',
                'repo_mode'        => 'required',
                'deploy_from'      => 'required',
                'release_user'     => 'required',
                'release_to'       => 'required',
                'release_library'  => 'required',
                'keep_version_num' => 'required',
            ];
            $this->validator($rules);
            $data = $this->request->all();
            $this->service->edit($data);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 删除用户
     */
    public function delete()
    {
        try {
            $rules = ['id' => 'required'];
            $this->validator($rules);
            $id = $this->request->input('id');
            $this->service->delete($id);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 阿里云SLB端口号
     */
    public function slbPort()
    {
        try {
            $rules = [
                'aliyun_id'     => 'required',
                'aliyun_region' => 'required'
            ];
            $this->validator($rules);
            $aliyunId     = $this->request->input('aliyun_id');
            $aliyunRegion = $this->request->input('aliyun_region');
            $ports        = $this->service->aliyunPort($aliyunId, $aliyunRegion);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $ports);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 项目检测
     */
    public function detection()
    {
        try {
            $rules = ['id' => 'required',];
            $this->validator($rules);
            $projectId = $this->request->input('id');
            $result    = $this->service->detection($projectId);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 项目分类
     */
    public function projectClassification()
    {
        try {
            $result = $this->service->classification();
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 线上md5文件校验
     */
    public function mdfCheck()
    {
        try {
            $rules = [
                'project_id' => 'required',
                'file'       => 'required',
            ];
            $this->validator($rules);
            $projectId = $this->request->input('project_id');
            $file      = $this->request->input('file');
            $result    = $this->service->md5($projectId, $file);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }
}
