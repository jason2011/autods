<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.03.30
 */

namespace App\Controller\Http;

use App\Constants\Constants;
use App\Library\Traits\Helper;
use App\Model\User;
use App\Service\UserService;
use Exception;
use Hyperf\Di\Annotation\Inject;

class UserController extends BaseController
{
    /**
     * @Inject
     * @var UserService
     */
    protected $service;

    /**
     * 获取用户列表
     */
    public function users()
    {
        try {
            $result = User::query()
                ->select(['user_id', 'nick_name as name', 'email', 'username', 'created_at', 'avatar'])
                ->where('user_id', '<>', 1)
                ->get();
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 获取当前用户基本信息
     *
     * @throws Exception
     */
    public function info()
    {
        $result = $this->service->info();
        return $this->response->showResults(Constants::RETURN_SUCCESS, 'SUCCESS', $result);
    }

    /**
     * 用户分页
     */
    public function list()
    {
        try {
            $page     = $this->request->input('page', 1);
            $pageSize = $this->request->input('pageSize', 10);

            $searchKey   = $this->request->input('search_key', '');
            $searchValue = $this->request->input('search_value', '');
            $condition   = [];

            if ($searchKey && $searchValue) {
                $key         = ($searchKey == 'name') ? 'nick_name' : 'email';
                $condition[] = [$key, 'like', '%' . trim($searchValue) . '%'];
            }
            $result = $this->service->list($page, $pageSize, $condition);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 新增用户
     */
    public function store()
    {
        try {
            $rules = [
                'username'  => 'required',
                'nick_name' => 'required',
                'email'     => 'required',
            ];
            $this->validator($rules);
            $data = $this->combine(array_keys($rules));
            $this->service->add($data);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 更新用户
     */
    public function update()
    {
        try {
            $rules = [
                'id'        => 'required',
                'username'  => 'required',
                'nick_name' => 'required',
                'email'     => 'required',
            ];
            $this->validator($rules);
            $data = $this->combine(array_keys($rules));
            $this->service->edit($data);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 删除用户
     */
    public function delete()
    {
        try {
            $rules = ['id' => 'required'];
            $this->validator($rules);
            $id = $this->request->input('id');
            $this->service->delete($id);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 修改个人信息
     */
    public function change()
    {
        try {
            $token    = $this->request->input(Constants::TOKEN) ?? ($this->request->header(Constants::X_TOKEN) ?? '');
            $nickName = $this->request->input('nick_name', '');
            $password = $this->request->input('password', '');
            $this->service->changeInfo($token, $nickName, $password);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }
}
