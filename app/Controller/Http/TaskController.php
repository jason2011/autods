<?php
/**
 * Copyright (c) 2020
 * 摘    要：上线单
 * 作    者：san
 * 修改日期：2020.04.11
 */

namespace App\Controller\Http;

use App\Constants\Constants;
use App\Library\Clog\Log;
use App\Library\Traits\Helper;
use App\Service\TaskService;
use Hyperf\Di\Annotation\Inject;

class TaskController extends BaseController
{
    /**
     * @Inject()
     * @var TaskService
     */
    protected $service;

    /**
     * TaskController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 获取上线单列表
     */
    public function index()
    {
        try {
            $page        = $this->request->input('page', 1);
            $pageSize    = $this->request->input('pageSize', 10);
            $searchKey   = $this->request->input('search_key', '');
            $searchValue = $this->request->input('search_value', '');
            $condition   = [];
            if ($searchKey && $searchValue) {
                $key         = 'p.name';
                $condition[] = [$key, 'like', '%' . trim($searchValue) . '%'];
            }
            $result = $this->service->query($page, $pageSize, $condition);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 上线单明细
     */
    public function show()
    {
        try {
            $rules = ['id' => 'required',];
            $this->validator($rules);
            $id     = $this->request->input('id');
            $result = $this->service->show($id);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 新增上线单
     */
    public function store()
    {
        try {
            $rules = [
                'project_id'             => 'required',
                'title'                  => 'required',
                'branch'                 => 'required',
                'commit_id'              => 'required',
                'file_transmission_mode' => 'required',
            ];
            $this->validator($rules);
            $this->service->add($this->request->all());
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 更新上线单
     */
    public function update()
    {
        try {
            $rules = [
                'id'        => 'required',
                'username'  => 'required',
                'nick_name' => 'required',
                'email'     => 'required',
            ];
            $this->validator($rules);
            $data = $this->combine(array_keys($rules));
            $this->service->edit($data);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 删除上线单
     */
    public function delete()
    {
        try {
            $rules = ['id' => 'required'];
            $this->validator($rules);
            $id = $this->request->input('id');
            $this->service->delete($id);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 获取分支列表
     */
    public function branch()
    {
        try {
            $rules = ['id' => 'required'];
            $this->validator($rules);
            $id     = $this->request->input('id');
            $result = $this->service->branchList($id);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 获取tag列表
     */
    public function tag()
    {
        try {
            $rules = ['id' => 'required'];
            $this->validator($rules);
            $id     = $this->request->input('id');
            $result = $this->service->tagList($id);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 获取提交版本
     */
    public function version()
    {
        try {
            $rules = [
                'id'     => 'required',
                'branch' => 'required',
            ];
            $this->validator($rules);
            $id     = $this->request->input('id');
            $branch = $this->request->input('branch');
            $result = $this->service->commitList($id, $branch);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), $result);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 上线单审核
     */
    public function audit()
    {
        try {
            $rules = ['id' => 'required',];
            $this->validator($rules);
            $id = $this->request->input('id');
            $this->service->audit($id);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }

    /**
     * 生成回滚单
     */
    public function rollback()
    {
        try {
            $rules = ['id' => 'required',];
            $this->validator($rules);
            $id = $this->request->input('id');
            $this->service->rollback($id);
            return $this->response->showResults(Constants::RETURN_SUCCESS, t('message.12001'), []);
        } catch (\Exception $exception) {
            Log::exception($exception);
            return $this->response->showResults(Constants::SERVER_ERROR, $exception->getMessage(), []);
        }
    }
}
