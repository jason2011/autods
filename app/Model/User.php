<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.03.30
 */

namespace App\Model;

use Donjan\Permission\Traits\HasRoles;
use Hyperf\Database\Model\SoftDeletes;

/**
 * Class User
 *
 * @package App\Model
 */
class User extends Model
{
    use SoftDeletes;

    use HasRoles;

    protected $guard_name = 'web';

    const ADMIN_UID = 1;

    const USER_SESSION_KEY = 'autods_user:';

    const TOKEN_CACHE_TTL = 24 * 60 * 60;

    const DEFAULT_PASSWORD = '123456';

    public $timestamps = false;

    protected $dateFormat = 'U';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected     $table      = 'users';
    protected     $primaryKey = 'user_id';
    protected     $guarded    = ['last_login_at', 'updated_at'];
    protected     $fillable   = ['username', 'email', 'phone', 'password', 'sex', 'created_at'];
    protected     $hidden     = ['password', 'remember_token', 'updated_at'];
    protected     $attributes = ['status' => 1];
    public static $sex        = [0 => '未知', 1 => '男', 2 => '女',];

    /**
     * @param array $condition
     * @param string $fields
     *
     * @return array|null
     */
    public static function findByCondition($condition, $fields = '*')
    {
        return self::query()->select($fields)->where($condition)->first();
    }
}
