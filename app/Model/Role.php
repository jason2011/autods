<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.03.30
 */

namespace App\Model;

class Role extends \Donjan\Permission\Models\Role
{
    const ROLE_ADMIN     = 1;//超级管理员
    const ROLE_OWNER     = 2;//部门管理人
    const ROLE_MASTER    = 3;//部门审核人
    const ROLE_DEVELOPER = 4;//开发者

    protected $guard_name = 'web';
}
