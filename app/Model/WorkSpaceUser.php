<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.04.01
 */

namespace App\Model;

class WorkSpaceUser extends Model
{
    protected $table = 'workspace_user';

    protected $fillable = ['user_id', 'role_id', 'workspace_id'];
}
