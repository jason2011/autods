<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.04.14
 */

namespace App\Model;

use App\Library\Clog\Log;
use App\Library\AutoDs\Command;

class Record extends Model
{
    protected $table = 'record';

    /**
     * 服务器权限检查
     */
    const ACTION_PERMSSION = 17;

    /**
     * 部署前置触发任务
     */
    const ACTION_PRE_DEPLOY = 34;

    /**
     * 本地代码更新
     */
    const ACTION_CLONE = 50;

    /**
     * 部署后置触发任务
     */
    const ACTION_POST_DEPLOY = 67;

    /**
     * 同步代码到服务器
     */
    const ACTION_SYNC = 84;

    /**
     * 更新完所有目标机器时触发任务，最后一个得是100
     */
    const ACTION_UPDATE_REMOTE = 100;

    public static function saveRecord($uid, Command $commandObj, $task_id, $action, $duration)
    {
        $commands = $commandObj->getExeCommand();

        foreach ($commands as $cmd) {
            $saveData = [
                'user_id'  => $uid,
                'task_id'  => $task_id,
                'status'   => (int)$commandObj->getExeStatus(),
                'action'   => $action,
                'command'  => var_export($cmd, true),
                'memo'     => $commandObj->getExeLog() ? substr(var_export($commandObj->getExeLog(), true), 0, 1500) : '',
                'duration' => $duration,
            ];

            Log::info('command', 'log', [$commandObj->getExeLog()]);

            $res = self::query()->insert($saveData);

            if (!$res) {
                continue;
            }
        }
    }
}
