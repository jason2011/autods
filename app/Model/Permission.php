<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.03.30
 */

namespace App\Model;

class Permission extends \Donjan\Permission\Models\Permission
{
    protected $guard_name = 'web';
}
