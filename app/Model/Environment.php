<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.04.03
 */

namespace App\Model;

use Hyperf\Database\Model\SoftDeletes;

class Environment extends Model
{
    use SoftDeletes;
    protected $table = 'environment';

    const PRO_ENV = 4;

    /**
     * 动态获取
     *
     * @param $level
     * @return mixed
     */
    public static function getAliasMap($level)
    {
        $result = self::query(true)->get();
        $tmp    = [];

        foreach ($result as $value) {
            $tmp[$value['id']] = $value['alias'];
        }

        return $tmp[$level];
    }

    public static function getNamesMap($level)
    {
        $result = self::query(true)->get();
        $tmp    = [];

        foreach ($result as $value) {
            $tmp[$value['id']] = $value['name'];
        }

        return $tmp[$level];
    }
}
