<?php
/**
 * Copyright (c) 2020
 * 摘    要：
 * 作    者：san
 * 修改日期：2020.04.10
 */

use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;
use Psr\Container\ContainerInterface;

/**
 * 获取Container
 */
if (!function_exists('app')) {
    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param null|mixed $id
     * @return mixed|ContainerInterface
     */
    function app($id = null)
    {
        $container = ApplicationContext::getContainer();
        if ($id) {
            return $container->get($id);
        }
        return $container;
    }
}

/**
 * 控制台日志
 */
if (!function_exists('error')) {
    function error($args)
    {
        return app()->get(StdoutLoggerInterface::class)->error(is_array($args) ? json_encode($args) : $args);
    }
}

/**
 * 控制台日志
 */
if (!function_exists('info')) {
    function info($args)
    {
        return app()->get(StdoutLoggerInterface::class)->info(is_array($args) ? json_encode($args) : $args);
    }
}

/**
 * redis 客户端实例
 */
if (!function_exists('redis')) {
    function redis($poll = 'default', $db = 0)
    {
        $redis = app()->get(RedisFactory::class)->get($poll);

        $redis->select($db);

        return $redis;
    }
}

/**
 * 生成唯一的Token
 */
if (!function_exists('generateToken')) {
    function generateToken()
    {
        return md5(uniqid() . microtime());
    }
}

/**
 * 转换成utf8
 *
 * @param $text
 * @return string
 */
if (!function_exists('convert2Utf8')) {
    function convert2Utf8($text)
    {
        $encoding = mb_detect_encoding($text, mb_detect_order(), false);
        if ($encoding == "UTF-8") {
            $text = mb_convert_encoding($text, 'UTF-8', 'UTF-8');
        }

        return iconv(mb_detect_encoding($text, mb_detect_order(), false), "UTF-8//IGNORE", $text);
    }
}

/**
 * 字符串转换成数组
 *
 * @param $string
 * @param $delimiter
 * @return array
 */
if (!function_exists('str2arr')) {
    function str2arr($string, $delimiter = PHP_EOL)
    {
        $items = explode($delimiter, $string);
        foreach ($items as $key => &$item) {
            $pos = strpos($item, '#');
            if ($pos === 0) {
                unset($items[$key]);
                continue;
            }
            if ($pos > 0) {
                $item = substr($item, 0, $pos);
            }
            $item = trim($item);
            if (empty($item)) {
                unset($items[$key]);
            }
        }
        return $items;
    }
}

/**
 * 返回提示
 *
 * @param $key
 * @return mixed
 */
if (!function_exists('t')) {
    function t($key, $replace = [])
    {
        list($config, $code) = explode('.', $key);

        $string = config($config)[$code];

        return vsprintf($string, $replace);
    }
}

/**
 * 对象转数组
 *
 * @param $object
 * @param bool $recursive
 * @return array
 */
if (!function_exists('object2array')) {
    function object2array($object, $recursive = true)
    {
        if (is_array($object)) {
            if ($recursive) {
                foreach ($object as $key => $value) {
                    if (is_array($value) || is_object($value)) {
                        $object[$key] = object2array($value);
                    }
                }
            }
            return $object;
        } elseif (is_object($object)) {
            $result = [];
            foreach ($object as $key => $value) {
                $result[$key] = $value;
            }
            return $recursive ? object2array($result) : $result;
        } else {
            return [$object];
        }
    }
}