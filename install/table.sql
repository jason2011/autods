SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for autods_environment
-- ----------------------------
DROP TABLE IF EXISTS `autods_environment`;
CREATE TABLE `autods_environment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `alias` varchar(32) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for autods_model_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `autods_model_has_permissions`;
CREATE TABLE `autods_model_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `autods_permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for autods_model_has_roles
-- ----------------------------
DROP TABLE IF EXISTS `autods_model_has_roles`;
CREATE TABLE `autods_model_has_roles` (
  `role_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `autods_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for autods_permissions
-- ----------------------------
DROP TABLE IF EXISTS `autods_permissions`;
CREATE TABLE `autods_permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名称',
  `type` smallint(6) DEFAULT '1' COMMENT '1-菜单 2-按钮',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `sort` smallint(6) NOT NULL DEFAULT '1' COMMENT '排序，数字越大越在前面',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for autods_project
-- ----------------------------
DROP TABLE IF EXISTS `autods_project`;
CREATE TABLE `autods_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(21) unsigned NOT NULL COMMENT '添加项目的用户id',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'master' COMMENT '项目名字',
  `workspace_id` int(11) DEFAULT '1' COMMENT '所属空间',
  `level` int(11) NOT NULL COMMENT '项目环境',
  `status` smallint(1) NOT NULL DEFAULT '1' COMMENT '状态0：无效，1有效',
  `version` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '线上当前版本，用于快速回滚',
  `gray_version` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '灰度线上当前版本，用于快速回滚',
  `repo_url` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'git地址',
  `repo_username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '版本管理系统的用户名，一般为svn的用户名',
  `repo_password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '版本管理系统的密码，一般为svn的密码',
  `repo_mode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'branch' COMMENT '上线方式：branch/tag',
  `repo_type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'git' COMMENT '上线方式：git/svn',
  `deploy_from` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '宿主机存放clone出来的文件',
  `excludes` text COLLATE utf8mb4_unicode_ci COMMENT '要排除的文件',
  `release_user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '目标机器用户',
  `release_to` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '目标机器的目录，相当于nginx的root，可直接web访问',
  `release_library` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '目标机器版本发布库',
  `aliyun_sync` tinyint(4) DEFAULT '0' COMMENT '0-未开启\n1-开启阿里云host同步',
  `aliyun_id` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '阿里云负载均衡实例ID',
  `aliyun_region` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT 'cn-shanghai' COMMENT '阿里云负载均衡地域',
  `aliyun_listen_port` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '阿里云监听端口号',
  `hosts` text COLLATE utf8mb4_unicode_ci COMMENT '目标机器列表',
  `is_open_gray` tinyint(4) DEFAULT '0' COMMENT '0-关闭 1-开启',
  `gray_hosts` text COLLATE utf8mb4_unicode_ci COMMENT '目标机器列表',
  `pre_deploy` text COLLATE utf8mb4_unicode_ci COMMENT '部署前置任务',
  `post_deploy` text COLLATE utf8mb4_unicode_ci COMMENT '同步之前任务',
  `pre_release` text COLLATE utf8mb4_unicode_ci COMMENT '同步之前目标机器执行的任务',
  `post_release` text COLLATE utf8mb4_unicode_ci COMMENT '同步之后目标机器执行的任务',
  `post_release_delay` int(11) NOT NULL DEFAULT '0' COMMENT '每台目标机执行post_release任务间隔/延迟时间 单位:秒',
  `audit` smallint(1) DEFAULT '0' COMMENT '是否需要审核任务0不需要，1需要',
  `ansible` smallint(3) NOT NULL DEFAULT '0' COMMENT '是否启用Ansible 0关闭，1开启',
  `keep_version_num` int(3) NOT NULL DEFAULT '20' COMMENT '线上版本保留数',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for autods_record
-- ----------------------------
DROP TABLE IF EXISTS `autods_record`;
CREATE TABLE `autods_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(21) unsigned NOT NULL COMMENT '用户id',
  `task_id` bigint(21) NOT NULL COMMENT '任务id',
  `status` smallint(1) NOT NULL DEFAULT '1' COMMENT '状态1：成功，0失败',
  `action` int(3) unsigned DEFAULT '10' COMMENT '任务执行到的阶段',
  `command` text COLLATE utf8mb4_unicode_ci COMMENT '运行命令',
  `duration` int(10) DEFAULT '0' COMMENT '耗时，单位ms',
  `memo` text COLLATE utf8mb4_unicode_ci COMMENT '日志/备注',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for autods_role_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `autods_role_has_permissions`;
CREATE TABLE `autods_role_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `autods_permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `autods_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for autods_roles
-- ----------------------------
DROP TABLE IF EXISTS `autods_roles`;
CREATE TABLE `autods_roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for autods_task
-- ----------------------------
DROP TABLE IF EXISTS `autods_task`;
CREATE TABLE `autods_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(21) unsigned NOT NULL COMMENT '用户id',
  `project_id` int(11) NOT NULL DEFAULT '0' COMMENT '项目id',
  `workspace_id` int(11) DEFAULT NULL COMMENT '部门id',
  `action` smallint(1) NOT NULL DEFAULT '0' COMMENT '0全新上线，2回滚',
  `status` smallint(1) NOT NULL DEFAULT '0' COMMENT '状态0：新建提交，1审核通过，2审核拒绝，3上线完成，4上线失败',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '上线的软链号',
  `link_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '上线的软链号',
  `ex_link_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '上一次上线的软链号',
  `commit_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'git commit id',
  `branch` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'master' COMMENT '选择上线的分支',
  `file_transmission_mode` smallint(3) NOT NULL DEFAULT '1' COMMENT '上线文件模式: 1.全量所有文件 2.指定文件列表',
  `file_list` text COLLATE utf8mb4_unicode_ci COMMENT '文件列表，svn上线方式可能会产生',
  `enable_rollback` int(1) NOT NULL DEFAULT '1' COMMENT '能否回滚此版本:0no 1yes',
  `gray_push` tinyint(4) DEFAULT '0' COMMENT '0-关闭 1-开启',
  `auditor` int(11) DEFAULT '0' COMMENT '审核人ID',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for autods_users
-- ----------------------------
DROP TABLE IF EXISTS `autods_users`;
CREATE TABLE `autods_users` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '邮箱',
  `nick_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` tinyint(4) NOT NULL DEFAULT '1',
  `avatar` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'https://file.iviewui.com/dist/a0e88e83800f138b94d2414621bd9704.png' COMMENT '头像',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '''''',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0为禁用，1为正常',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_username_unique` (`username`),
  KEY `user_status_index` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for autods_workspace
-- ----------------------------
DROP TABLE IF EXISTS `autods_workspace`;
CREATE TABLE `autods_workspace` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for autods_workspace_user
-- ----------------------------
DROP TABLE IF EXISTS `autods_workspace_user`;
CREATE TABLE `autods_workspace_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `workspace_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

SET FOREIGN_KEY_CHECKS = 1;
